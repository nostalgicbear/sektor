﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatCollider : MonoBehaviour
{
    public AudioSource ballHit;

    // Start is called before the first frame update
    void Start()
    {
        ballHit = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Ball")
        {
            ballHit.PlayOneShot(ballHit.clip, 1f);
        }
    }
}
