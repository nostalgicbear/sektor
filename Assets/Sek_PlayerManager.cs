﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Valve.VR.InteractionSystem;
using Valve.VR;

namespace Sektor
{
    public class Sek_PlayerManager : NetworkBehaviour
    {
        public Camera playerCamera;
        public Hand leftHand;
        public Hand rightHand;
        public SteamVR_Behaviour_Pose leftHandPose;
        public SteamVR_Behaviour_Pose rightHandPose;

        [SyncVar]
        private int _playerNumber = 0;

        [SerializeField]
        Sek_PlayerController _playerController;

        public override void OnStartLocalPlayer()
        {
            if (isServer)
            {
                Debug.Log("<color=blue>I am the server</color>");
            }
            Debug.Log("OnStartLocalPlayer called");
            if (isLocalPlayer)
            {
                Debug.Log("OnStartLocalPlayer called: is Local player");

                if (_playerController)
                {
                    playerCamera.enabled = true;
                    leftHand.enabled = true;
                    rightHand.enabled = true;
                    leftHandPose.enabled = true;
                    rightHandPose.enabled = true;

                    _playerController.enabled = true;
                }


                gameObject.name = "LocalPlayer: Client: " + (_playerNumber + 1).ToString();
                Sek_LocalPlayerManager.Instance.localPlayerController = _playerController;
                //if (Sek_GameManager.Instance.player1Controller == null)
                //{
                //    Sek_GameManager.Instance.player1Controller = _playerController;
                //}
                //else {
                //    Sek_GameManager.Instance.player2Controller = _playerController;
                //}
            }
            else
            {
                gameObject.name = "Other Player";
            }
        }


        public void Start()
        {

            if (isLocalPlayer)
            {

            }

        }
    }
}