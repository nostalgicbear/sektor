﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Sektor;
using System;

public class Sek_GameManager : NetworkBehaviour
{
    private static Sek_GameManager _instance;
    public static Sek_GameManager Instance { get { return _instance; } }
    List<Prototype_PlayerController> _playerMovementControllers;

    [SerializeField]
    private int player1Score = 0;
    [SerializeField]
    private int player2Score = 0;
    [SerializeField]
    private int player1ID = -1;
    [SerializeField]
    private int player2ID = -1;

    // Start is called before the first frame update
    void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);

        ResetScore();
    }
   

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnStartClient()
    {
        UpdatePlayerMovementControllerList();
    }

    /// <summary>
    /// Called when a client connects. Adds the player controller to the list of known player controllers
    /// </summary>
    private void UpdatePlayerMovementControllerList()
    {
        Debug.Log("<color=blue> A client has connected. Adding playerController to the list</color>");
        _playerMovementControllers = new List<Prototype_PlayerController>(FindObjectsOfType<Prototype_PlayerController>());
    }


    //Called by the server internally to reset the score
    private void ResetScore()
    {
        player1Score = 0;
        player2Score = 0;
    }

    public void IncreaseScore()
    {      
        RpcUpdateScore();
        
    }

    [ClientRpc]
    public void RpcUpdateScore()
    {
            Sek_LocalPlayerManager.Instance.UpdateScore();
    }

    [ClientRpc]
    private void RpcPlayer1_UpdateScore()
    {
        player1Score += 10;
    }

    [ClientRpc]
    private void RpcPlayer2_UpdateScore()
    {
        player2Score += 10;
    }
}
