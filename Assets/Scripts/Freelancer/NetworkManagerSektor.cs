﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkManagerSektor : NetworkManager
{

    public Transform player1_Start;
    public Transform player2_Start;
    public GameObject ballPrefab;
    GameObject ball;
    public override void OnServerAddPlayer(NetworkConnection conn, AddPlayerMessage extraMessage)
    {
        // add player at correct spawn position
        Transform start = numPlayers == 0 ? player1_Start : player2_Start;

        GameObject player = Instantiate(playerPrefab, start.position, start.rotation);
        NetworkServer.AddPlayerForConnection(conn, player);
        

        // spawn ball if two players
        if (numPlayers == 2)
        {
            Debug.LogError("2 people in game");
            ball = Instantiate(ballPrefab);
            NetworkServer.Spawn(ball);
        }
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {

        // call base functionality (actually destroys the player)
        base.OnServerDisconnect(conn);
    }
}
