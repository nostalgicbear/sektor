﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Sektor
{
    public class Sek_CollisionVisualizer : MonoBehaviour
    {
        public Vector3 bounds;
        private void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, bounds);
        }
    }
}
