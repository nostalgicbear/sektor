﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Prototype_PlayerController : NetworkBehaviour
{
    public Prototype_MovePlayer movePlayer;
    public Camera cam;

    public BlockCollision lastBlockHit;
    // Start is called before the first frame update
    void Start()
    {
        if(isLocalPlayer)
        {
            Debug.LogError("Start function of PlayerController");

            movePlayer.enabled = true;
            cam.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public override void OnStartLocalPlayer()
    {
        if (isLocalPlayer)
        {
            
            Sek_LocalPlayerManager.Instance.pc = this;

            if (FindObjectsOfType<Prototype_PlayerController>().Length <= 1)
            {
                Sek_LocalPlayerManager.Instance.playerNumber = 1;
            }
            else {
                Sek_LocalPlayerManager.Instance.playerNumber = 2;
            }

            Sek_LocalPlayerManager.Instance.SetScoreCanvasPosition();
            gameObject.name = "Me: Player: "+ Sek_LocalPlayerManager.Instance.playerNumber;



        }
        else
        {
            gameObject.name = "Other Player";
        }
    }

    [Command]
    public void CmdTellServerHit()
    {
        Sek_GameManager.Instance.IncreaseScore();
    }

    [ClientRpc]
    public void RpcUpdateLastBlockHit(int idOfCube)
    {
        BlockCollision[] blocks = FindObjectsOfType<BlockCollision>();
        List<BlockCollision> allBlocks = new List<BlockCollision>();
        allBlocks.AddRange(blocks);
        foreach(BlockCollision bc in allBlocks)
        {
            if(bc.id == idOfCube)
            {
                lastBlockHit = bc;
            }
        }
    }

    [ClientRpc]
    public void RpcUpdateBlockHealth()
    {
        lastBlockHit.HitBlock();
    }


}
