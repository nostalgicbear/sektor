﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreCollision : MonoBehaviour
{
    public AudioSource ballHit;
    Light lol;
    public Material[] material;
    Renderer rend;
    private bool destroyVar;

    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.sharedMaterial = material[0];
        lol = GetComponent<Light>();
        ballHit = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(destroyVar && !ballHit.isPlaying)
        {
            Destroy(this.gameObject);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.name == "Ball")
        {
            ballHit.PlayOneShot(ballHit.clip, 1f);
            startFading();
            Destroy(GetComponent<MeshCollider>());
            destroyVar = true;
        }
    }

    IEnumerator FadeOut()
    {
        for(float f = 1f; f >= -0.05f; f -= 0.05f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend.material.color = c;

            yield return new WaitForSeconds(0.05f);
        }
    }

    public void startFading()
    {
        StartCoroutine("FadeOut");
    }
}
