﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BallPhysics : NetworkBehaviour
{
    public float MaxVelocity = 26f;
    public float MinVelocity = 10f;
    public GameObject ball;
    public Material dead;
    public Material alive; 
    Renderer rend;
    public AudioSource hit;
    private Rigidbody localRgb;
    private int waitStep;
    public int shielddeterm;

    // Start is called before the first frame update
    void Start()
    {
        localRgb = GetComponent<Rigidbody>();
        rend = GetComponent<Renderer>();
        hit = GetComponent<AudioSource>();

        localRgb.velocity = new Vector3(2.0f, 3.6f, 6.0f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(localRgb.velocity.magnitude > MaxVelocity)
        {
            localRgb.velocity = Vector3.ClampMagnitude(localRgb.velocity, MaxVelocity);
        }
        if(localRgb.velocity.magnitude < MinVelocity)
        {
            localRgb.velocity = Vector3.ClampMagnitude(localRgb.velocity, MinVelocity);
            if(localRgb.velocity.magnitude < MinVelocity && localRgb.velocity.magnitude > 0f)
            {
                whenMovingSlow();    
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "at_mine_LOD0 Variant")
        {
            localRgb.velocity = new Vector3(10f, 0f, 0f);
        }
        else if(collision.gameObject.name == "Cube" || collision.gameObject.name == "Core" || collision.gameObject.name == "red_bat")
        {

        }
        else
        {
            hit.PlayOneShot(hit.clip, 1f);
        }
    }

    IEnumerator whenMovingSlow()
    {   
        if(localRgb.velocity.magnitude < MinVelocity && localRgb.velocity.magnitude > 0f && waitStep < 3)
        {
            waitStep++;
            yield return new WaitForSecondsRealtime(1f);
        }
        else if(localRgb.velocity.magnitude < MinVelocity && localRgb.velocity.magnitude > 0f && waitStep > 3)
        {
            yield return new WaitForSecondsRealtime(1f);
        }
        else if(localRgb.velocity.magnitude > MinVelocity && localRgb.velocity.magnitude > 0f)
        {

        }
    }

    public void slowRoutine()
    {
        StartCoroutine(whenMovingSlow());  
    }

}
