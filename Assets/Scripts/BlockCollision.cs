﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCollision : MonoBehaviour
{
    public int id;
    public int owner;
    public AudioSource[] cubeSounds;
    public Material[] material;
    Renderer rend;
    [SerializeField]
    private int cubeHealth = 3;

    bool canReeduceHealth = true;


    void Start()
    {
        rend = GetComponent<Renderer>();
        rend.sharedMaterial = material[0];
    }

    void Update()
    {

    }

    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.tag == Sektor.Sek_TagManager.BALL)
        {
            UpdateCollisionIfServer();                
        }
    }

    /// <summary>
    /// Updates the collision if this is the server player
    /// </summary>
    private void UpdateCollisionIfServer()
    {
        
            Sek_LocalPlayerManager.Instance.pc.RpcUpdateLastBlockHit(this.id);
            Sek_LocalPlayerManager.Instance.pc.RpcUpdateBlockHealth();
        
    }

    public void HitBlock()
    {
        ReduceCubeHealth();
    }


    /// <summary>
    /// Reduces cube health by 1 and updates colour status
    /// </summary>
    public void ReduceCubeHealth()
    {
        Debug.LogError("REUDCE CUBE HEALTH");
        if(!canReeduceHealth)
        {
            return;
        }

        canReeduceHealth = false;
        StartCoroutine(CoolDown());
        cubeHealth -= 1;
       
        UpdateCubeStatus();
        Hit();
    }

    private void UpdateCubeStatus()
    {
        switch (cubeHealth)
        {
            case 0:
                DestroyCube();
                break;

            case 1:
                rend.sharedMaterial = material[2];
                break;

            case 2:
                rend.sharedMaterial = material[1];
                break;
        }

    }

    /// <summary>
    /// Turn off cube a sits less expensive than destroying the cube
    /// </summary>
    private void DestroyCube()
    {
        gameObject.SetActive(false);

    }

    private void Hit()
    {
        Debug.Log("HIT called on block. Going to tell server block is hit");
        Sek_LocalPlayerManager.Instance.pc.CmdTellServerHit();
        
    }

    IEnumerator CoolDown()
    {
        yield return new WaitForSeconds(0.2f);
        canReeduceHealth = true;
    }

}