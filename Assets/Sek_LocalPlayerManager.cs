﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.UI;
using System;

public class Sek_LocalPlayerManager : MonoBehaviour
{
    private static Sek_LocalPlayerManager _instance;
    public static Sek_LocalPlayerManager Instance { get { return _instance; } }
    [Header("Player")]
    public Sektor.Sek_PlayerController localPlayerController;
    public Prototype_PlayerController pc;
    public int playerNumber;


    [Header("Canvas variables")]
    public Transform canvasTransform;
    public Transform p1CanvasPos;
    public Transform p2CanvasPos;



    private int p1_ScoreNumber;
    private int p2_ScoreNumber;
    public Text p1Score;
    public Text p2Score;

    
    void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(this.gameObject);


    }

    public void UpdateScore()
    {
        if (pc.lastBlockHit.owner == 1)
        {
            UpdatePlayer2Score();
        }
        else {
            UpdatePlayer1Score();
        }
    }

     void UpdatePlayer1Score()
    {
        p1_ScoreNumber += 10;
        p1Score.text = p1_ScoreNumber.ToString();
    }

     void UpdatePlayer2Score()
    {
        p2_ScoreNumber += 10;
        p2Score.text = p2_ScoreNumber.ToString();
    }


    public void SetScoreCanvasPosition()
    {
        Debug.Log("SetScoreCanvas: player num is " + playerNumber);
        if (playerNumber == 1)
        {
            canvasTransform.position = p1CanvasPos.position;
        }
        else {
            canvasTransform.position = p2CanvasPos.position;
        }
    }
}
