﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class Prototype_MovePlayer : NetworkBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(!isLocalPlayer)
        {
            return;
        }
        if(Input.GetKey(KeyCode.E))
        {
            transform.Rotate(Vector3.up, 5f);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            transform.Rotate(Vector3.up, -5f);
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward * Time.deltaTime * 3.0f);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-Vector3.forward * Time.deltaTime * 3.0f);
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * Time.deltaTime * 3.0f);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * Time.deltaTime * 3.0f);
        }
    }

}
