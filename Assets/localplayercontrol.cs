﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class localplayercontrol : NetworkBehaviour
{
    public GameObject SteamVRObjects;
    public Transform leftHand;
    public Transform rightHand;
    public Camera leftEye;
    public Camera rightEye;
    Vector3 pos;
    void Start () {
        pos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            Destroy(SteamVRObjects);
        }
        else
        {
               if(leftEye.tag != "MainCamera")
            {
                leftEye.tag = "MainCamera";
                leftEye.enabled = true;
            }
                if (rightEye.tag != "MainCamera")
            {
                rightEye.tag = "MainCamera";
                rightEye.enabled = true;
            }

             
        }
    }
}
